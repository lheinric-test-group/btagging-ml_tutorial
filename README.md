# b-tagging Machine Learning Tutorial

## Quick Start
1. [https://hub.cern.ch](https://hub.cern.ch) (you need to accept the certificate exception) and use the `atlas-ml-cpu` profile
2. Open a terminal on the Hub and type `bash` followed by `kinit <CERNLOGIN>` - will enable eos access 
3. Clone this repository `git clone https://gitlab.cern.ch/mguth/btagging-ml_tutorial.git` (All files not stored in your eos will be deleted when restarting the server!)
4. Start exploring the notebooks by opening them on the tab on the left hand side.

More detailed instructions below.

## Technical Setup
There are two ways to run the tutorial

### OPTION 1 Using the JupyterHub from CERN (RECOMMENDED)

* Navigate to this web page [https://hub.cern.ch](https://hub.cern.ch) (you need to accept the certificate exception).
* Sign in with your CERN account - to access the resources you need to be on this e-group: hn-atlas-flavourtagperformance (greped the memebers on 04.06.2019)
* Choose the `atlas-ml-cpu` profile 
<!--* (`atlas-gpu` can be used as well however there are only a few GPUs available)-->
* Open a terminal on the JupyterHub and type
```
bash
kinit <CERNLOGIN>
```


### OPTION 2 Using lxplus with port forwarding

* Log into lxplus
```
<CERNLOGIN>@lxplus.cern.ch
```
* Execute the following command to run docker image
```
singularity exec -B /eos docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-cpu/ml-cpu:latest bash
```
with the `--pwd` flag you can specify a specific startup folder
* You are now in the docker image and can start `jupyter-lab` via
```
jupyter-lab --no-browser --port <PORT>
```
please use a port with a number greater than 1023 (it won't work when 2 people use the same port at a time).

On your local machine you need to redirect the port, open a new terminal and run
```
ssh -N -f -L localhost:8888:localhost:<PORT> <CERNLOGIN>@lxplus<LXPLUSMACHINE>.cern.ch
```
Afterwards you can access the jupyter-lab via your browser by typing as url [http://localhost:8888](http://localhost:8888)

You will be asked to provide a token which can be obtained from the terminal in which you started jupyter-lab where you see a line like this
```
http://localhost:7760/?token=7d41c1e979e62653221c6018b4a23eb44bb6035fa349173c
```
You need to copy paste the token (in this case `7d41c1e979e62653221c6018b4a23eb44bb6035fa349173c` - please use yours this one will not work).

## Getting started

<!--Please open a new terminal on your local machine and log into lxplus-->
<!--```-->
<!--ssh -Y <CERNLOGIN>@lxplus.cern.ch-->
<!--```-->
<!--navigate to your `/eos` homefolder (or a subfolder)-->
<!--```-->
<!--cd /eos/user/<firstLetterCERNLOGIN>/<CERNLOGIN>/-->
<!--```-->
<!--and copy the following folder-->
You can access the training files on eos:
`/eos/user/m/mguth/public/btagging-ml_tutorial_files`


Alternatively you can also directly access the files on the hub under the following path `/eos/home-m/mguth/public/btagging-ml_tutorial_files`.

Afterwards in Jupyterlab (either on the Hub or from lxplus as described above) open a Terminal.
You can navigate to your eos home via
```
cd /eos/home-<firstLetterCERNLOGIN>/<CERNLOGIN>/
```
Then check out the git repository 
```
git clone https://:@gitlab.cern.ch:8443/mguth/btagging-ml_tutorial.git
```
Inside the repository you will find different Jupyter notebooks.
N.B. all files NOT saved on your eos will be erased when restarting the jupyter server!!!!

The notebook `Sample-preparation.ipynb` contains the pre-processing for DIPS and DL1 and the `train*` notebooks are for DIPS and DL1 training.

The notebooks can be started by navigating to the folder from the sidebar where they are in and then right click on it and chose `Open`.


## Some useful options

* [https://hub.cern.ch/hub/home](https://hub.cern.ch/hub/home) - create and manage multiple named servers
* shutting down server: `jupyter notebook stop 8888`
* if you can't get eos working, try using `curl`, [to download the files directly](https://dguest-public.web.cern.ch/dguest-public/ftag/tutorial/)i.e. `curl https://dguest-public.web.cern.ch/dguest-public/ftag/tutorial/MC16d_Zprime-test-validation_sample-NN.h5 > MC16d_Zprime-test-validation_sample-NN.h5`

